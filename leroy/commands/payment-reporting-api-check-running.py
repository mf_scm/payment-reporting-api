import leroy, os, re, sys
# 
timeout = 60
port = int(os.environ['PAYMENT_REPORTING_API_PORT'])
ip = os.environ['PAYMENT_REPORTING_API_HOST']
if leroy.checkPortListening(ip,port,timeout):
 print("[INFO] precheck-server is awake and listening on port: " + str(port)) 
else:
 print("[ERROR] precheck-server has not started and  port: " + str(port) + " is still listenening, even after " + str(timeout) + " seconds") 
 sys.exit(1)