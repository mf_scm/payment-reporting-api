#!/bin/bash -e
alias cp='cp'

if [ ! -d ${CONFIG_TMP_DIR}/.${COMPONENT_NAME}-configs/${ENVIRONMENT_NAME}-${ID} ]; then
	echo "[ERROR] Very sad, I am missing: ${CONFIG_TMP_DIR}/.${COMPONENT_NAME}-configs/${ENVIRONMENT_NAME}-${ID} can not copy configs."
	exit 1
fi

function copyConfig {
	BASE=${CONFIG_TMP_DIR}/${COMPONENT_NAME}-configs/${ENVIRONMENT_NAME}-${ID}
	RESOURCE=$1
	DEST=$2
	
	if [ -d $2 ]; then
		if [ ! -d $BASE/$1 ]; then
			echo "[ERROR] missing resource $1 as $BASE/$1"
			exit 1
		fi
		# echo "[INFO] $2 exists, copying resource $1 to $2 ..."
		cp -Rfv $BASE/$1/* $2
	else
		echo "[INFO] $2 does not exist on this server, skipping resource: $1"
	fi
}

echo "[INFO] Copying configurations from leroy generated configurations..."
if [ ! -d ${PAYMENT_REPORTING_API_BASEDIR} ]; then
	mkdir -pv ${PAYMENT_REPORTING_API_BASEDIR}
fi
cp -rvf ${CONFIG_TMP_DIR}/.${COMPONENT_NAME}-configs/${ENVIRONMENT_NAME}-${ID}/init.d/${COMPONENT_NAME}-init.sh /etc/init.d/${COMPONENT_NAME}-init-${ENVIRONMENT_NAME}.sh

chmod -v u+x /etc/init.d/${COMPONENT_NAME}-init-${ENVIRONMENT_NAME}.sh
/sbin/chkconfig --add /etc/init.d/${COMPONENT_NAME}-init-${ENVIRONMENT_NAME}.sh