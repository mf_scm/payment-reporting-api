import leroy, os, re, sys
# 
timeout = 15
port = int(os.environ['PAYMENT_REPORTING_API_PORT'])
ip = os.environ['PAYMENT_REPORTING_API_HOST']
if leroy.checkPortNotListening(ip,port,timeout):
 print("[INFO] precheck-server is no longer listening on port: " + str(port)) 
else:
 print("[ERROR] precheck-server is not stopping and port: " + str(port) + " is still listenening, even after " + str(timeout) + " seconds") 
 sys.exit(1)