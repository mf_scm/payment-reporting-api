#!/bin/bash

PORT=${PAYMENT_REPORTING_API_PORT}
HOST=${PAYMENT_REPORTING_API_HOST}
LOG_FILE=${PAYMENT_REPORTING_API_BASEDIR}/${COMPONENT_NAME}.log

function forceKill {
	ps -e | grep ${COMPONENT_NAME} | grep $PORT | grep -v grep | awk '{print $1}' | xargs kill
}
case "$1" in
	start)
		echo "****** Starting ${COMPONENT_NAME} with host: $HOST on port: $PORT ******"
		nohup ${PAYMENT_REPORTING_API_JAVADIR}/bin/java -jar -Dpayment.reporting.api.host=$HOST -Dpayment.reporting.api.port=$PORT ${PRECHECK_SERVER_BASEDIR}/${COMPONENT_NAME}.jar > $LOG_FILE &
		echo $! > ${PAYMENT_REPORTING_API_BASEDIR}/${COMPONENT_NAME}.pid
	;;
	stop)
		echo "****** Stopping precheck-server on host: $HOST on port: $PORT ******"
		if [ -f ${PAYMENT_REPORTING_API_BASEDIR}/${COMPONENT_NAME}.pid ]; then
			kill `cat ${PAYMENT_REPORTING_API_BASEDIR}/${COMPONENT_NAME}.pid`
			if [ $? -ne 0 ]; then
				echo "[INFO] Server didn't die with pid, attempting to kill based on process name and port"
				forceKill
			fi
		else
			echo "[INFO] Missing pidfile: ${PAYMENT_REPORTING_API_BASEDIR}/${COMPONENT_NAME}.pid trying to kill by process name and port"
			# forceKill
		fi
	;;
	*)
		echo "Usage: $0 {start|stop}"
	;;
esac
exit 0