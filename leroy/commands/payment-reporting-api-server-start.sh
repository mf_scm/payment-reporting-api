#!/bin/bash
if [ ! -f /etc/init.d/${COMPONENT_NAME}-init-${ENVIRONMENT_NAME}.sh ]; then
	echo "[ERROR] Missing: /etc/init.d/${COMPONENT_NAME}-init-${ENVIRONMENT_NAME}.sh"
	exit 1
fi
/bin/bash /etc/init.d/${COMPONENT_NAME}-init-${ENVIRONMENT_NAME}.sh start