#!/bin/bash

PORT=${PRECHECK_SERVER_PORT}
HOST=${PRECHECK_SERVER_HOST}
WSO2_LOC=${PRECHECK_WSO2_LOC}
WSO2_USER=${PRECHECK_WSO2_USER}
WSO2_PASS=${PRECHECK_WSO2_PASS}
LOG_FILE=${PRECHECK_SERVER_BASEDIR}/log/precheck-server.log
SERVER_USER=${PRECHECK_SERVER_USER}

# yum install shadow-utils -y
echo "[INFO] Checking if $SERVER_USER exists.."
id $SERVER_USER > /dev/null
if [ $? -ne 0 ]; then
	echo "[INFO] $SERVER_USER does not exist as a user on this machine, I will try to make it.."
	if [ $USER == "root" ]; then
		/usr/sbin/adduser $SERVER_USER --home-dir $PRECHECK_SERVER_BASEDIR
		if [ $? -eq 0 ]; then
			echo "[INFO] User $SERVER_USER created"
		else
			echo "[ERROR] Could not add user, please add: $SERVER_USER and rerun deployment"
			exit 1
		fi
	else
		echo "[INFO] Sorry, I am not root I can't add the user $SERVER_USER to the system automatically"
	fi
else
	echo "[INFO] Seems that $SERVER_USER has already been created and ready to go!"
fi

if [ ! -d ${PRECHECK_SERVER_BASEDIR}/log ]; then
	mkdir -p ${PRECHECK_SERVER_BASEDIR}/log
fi
if [ ! -d ${PRECHECK_SERVER_BASEDIR}/pid ]; then
	mkdir -p ${PRECHECK_SERVER_BASEDIR}/pid
fi
if [ ! -d ${PRECHECK_SERVER_BASEDIR}/jar ]; then
	mkdir -p ${PRECHECK_SERVER_BASEDIR}/jar
fi
chown -R $SERVER_USER ${PRECHECK_SERVER_BASEDIR}

function forceKill {
	ps -e | grep precheck-server | grep $PORT | grep -v grep | awk '{print $1}' | xargs kill
}
case "$1" in
	start)
		echo "****** Starting precheck-server with host: $HOST on port: $PORT ******"
		su - ${PRECHECK_SERVER_USER}
		export JAVA_HOME=${PRECHECK_SERVER_JAVADIR}
		${PRECHECK_SERVER_JAVADIR}/bin/java -jar -Djava.security.auth.login.config=${PRECHECK_SERVER_BASEDIR}/precheck-jaas.conf -Dprecheck.host=$HOST -Dprecheck.port=$PORT -Didentity.wso2.location=${PRECHECK_WSO2_LOC} -Didentity.wso2.admin.username=${PRECHECK_WSO2_USER} -Didentity.wso2.admin.password=${PRECHECK_WSO2_PASS} ${PRECHECK_SERVER_BASEDIR}/jar/precheck-server.jar > $LOG_FILE 2>&1 &
		echo $! > ${PRECHECK_SERVER_BASEDIR}/pid/precheck-server.pid
		logout
		exit 0
	;;
	stop)
		echo "****** Stopping precheck-server on host: $HOST on port: $PORT ******"
		PIDFILE=${PRECHECK_SERVER_BASEDIR}/pid/precheck-server.pid
		if [ -f $PIDFILE ]; then
			PID=`cat $PIDFILE`
			ps $PID
			if [ $? -eq 0 ]; then 
				kill `cat ${PRECHECK_SERVER_BASEDIR}/pid/precheck-server.pid`
				if [ $? -ne 0 ]; then
					echo "[INFO] Server didn't die with pid, attempting to kill based on process name and port"
					forceKill
				fi
			else
				echo "[INFO] Server is not running"
				exit 0
			fi
		else
			echo "[INFO] Missing pidfile: ${PRECHECK_SERVER_BASEDIR}/pid/precheck-server.pid trying to kill by process name and port"
			# forceKill
		fi
	;;
	*)
		echo "Usage: $0 {start|stop}"
	;;
esac
exit 0